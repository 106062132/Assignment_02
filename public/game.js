var flag_window;
var player;
var aliens;
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var nextEnemyAt;
var enemyDelay = 1500;
var leveltime;
var flag_ultimate;
var pasue_flag;
var nextpause;
var username;
var boss;
var bosslives;
var winflag;
var mainState = {
    preload: function() {
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('blackwindow', 'assets/window3.jpg');
        game.load.image('bg111', 'assets/bg.jpeg');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('invader', 'assets/enemy.png');
        // game.load.image('ship', 'assets/player.png');
        game.load.image('ship2', 'assets/love.png');
        game.load.image('ship3', 'assets/ultimate.png');
        game.load.spritesheet('kaboom', 'assets/explosion.png', 32, 32);
        game.load.spritesheet('ship','assets/spritesheet.png',145,125);
        game.load.image('starfield', 'assets/bg.jpg');
        game.load.image('enter', 'assets/enter.png');
        game.load.audio('boden', 'assets/player-fire.wav');
        game.load.audio('boden2', 'assets/enemy-fire.wav');
        game.load.image('pausee', 'assets/pause.png');
        game.load.image('boss', 'assets/boss.png');
        game.load.image('aim', 'assets/aim.png');
        // game.load.image('background', 'assets/bg.jpg');
      },

      create: function() {
        pasue_flag=1;
        flag_ultimate=1;
        leveltime=0.0;
        nextEnemyAt=0;
        flag_window=0;
        nextpause=0;
        bosslives=10;
        this.startgame = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.quit = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.continue = game.input.keyboard.addKey(Phaser.Keyboard.S);
        this.ultimate = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.voulumeup = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.voulumedown = game.input.keyboard.addKey(Phaser.Keyboard.W);
        this.pasue = game.input.keyboard.addKey(Phaser.Keyboard.P);

        game.physics.startSystem(Phaser.Physics.ARCADE);

        //  The scrolling starfield background
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

        //  Our bullet group
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(30, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);

        // The enemy's bullets
        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(30, 'enemyBullet');
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);

        //  The hero!
        player = game.add.sprite(0, 300, 'ship');
        player.animations.add('fly', [0, 1, 2, 3], 20, true);
        player.play('fly');
        player.anchor.setTo(0.5, 0.5);
        player.scale.setTo(0.7, 0.7); 
        game.physics.enable(player, Phaser.Physics.ARCADE);

        aim = game.add.sprite(0, -1000, 'aim');
        aim.anchor.setTo(0.5, 0.5);
        aim.scale.setTo(0.3, 0.3); 
        game.physics.enable(aim, Phaser.Physics.ARCADE);

        boss = game.add.sprite(0, -1000, 'boss');
        boss.anchor.setTo(0.5, 0.5);
        boss.scale.setTo(0.3, 0.3); 
        game.physics.enable(boss, Phaser.Physics.ARCADE);
        //  The baddies!
        aliens = game.add.group();
        aliens.enableBody = true;
        aliens.physicsBodyType = Phaser.Physics.ARCADE;

        //  The score
        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

        this.ultimatelogo = game.add.sprite(600,30, 'ship3');;
        this.ultimatelogo.anchor.setTo(0.5, 0.5);

        //  Lives
        lives = game.add.group();
        // game.add.text(game.world.width - 100, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });
        this.pauseButton = game.add.sprite(485, 5, 'pausee');
        this.pauseButton.inputEnabled = true;
        this.pauseButton.events.onInputUp.add(function () {this.game.paused = true;},this);
        this.game.input.onDown.add(function () {if(this.game.paused)this.game.paused = false;},this);
        //  Text
        this.black0 = game.add.sprite(0, 0, 'bg111');
        this.black0.scale.setTo(3, 4); 
        username = prompt("Please enter your name", "name");
        localStorage.setItem("playerName", username);
        // localStorage.getItem("playerName");
        this.enter = game.add.sprite(0, 550, 'enter');
        this.enter.scale.setTo(0.5, 0.5); 
        this.black = game.add.sprite(-10000, 0, 'blackwindow');

        stateText = game.add.text(380,100,' ', { font: '50px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        stateText2 = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
        stateText2.anchor.setTo(0.5, 0.5);
        stateText2.visible = false;

        for (var i = 0; i < 3; i++) 
        {
            var ship = lives.create(game.world.width - 100 + (40 * i), 30, 'ship2');
            ship.anchor.setTo(0.5, 0.5);
        }

        //  An explosion pool
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(this.setupInvader, this);

        //  And some controls to play the game with
        cursors = game.input.keyboard.createCursorKeys();
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        stateText3 = game.add.text(game.world.centerX,game.world.centerY-100,' ', { font: '32px Arial', fill: '#fff' });
        stateText3.anchor.setTo(0.5, 0);
        stateText3.visible = false;

        firebase.database().ref('game_score').push({
            username: localStorage.getItem("playerName") ,
            score: score
        }); 
    },
    createAliens: function () {

        // for (var y = 0; y < 4; y++)
        // {
        //     for (var x = 0; x < 10; x++)
        //     {
        //         var alien = aliens.create(x * 48, y * 50, 'invader');
        //         alien.anchor.setTo(0.5, 0.5);
        //         // alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        //         // alien.play('fly');
        //         alien.body.moves = false;
        //     }
        // }
    
        // aliens.x = 100;
        // aliens.y = 50;
    
        // //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        // var tween = game.add.tween(aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    
        // //  When the tween loops it calls descend
        // tween.onLoop.add(this.descend, this);
        if (nextEnemyAt<game.time.now && leveltime<=1) {
            nextEnemyAt = game.time.now + enemyDelay;
            var alien = aliens.create(780, game.rnd.integerInRange(100, 580), 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.body.velocity.x = -100;
          }
        if (nextEnemyAt<game.time.now && leveltime>1 && leveltime<=2) {
        nextEnemyAt = game.time.now + enemyDelay - 500;
        var alien = aliens.create(780, game.rnd.integerInRange(100, 580), 'invader');
        alien.anchor.setTo(0.5, 0.5);
        alien.body.velocity.x = -100;
        }
        if (nextEnemyAt<game.time.now && leveltime>2 && leveltime<=3) {
        nextEnemyAt = game.time.now + enemyDelay - 250;
        var alien = aliens.create(780, game.rnd.integerInRange(100, 580), 'invader');
        alien.anchor.setTo(0.5, 0.5);
        alien.body.velocity.x = -100;
        }
    },
    
    setupInvader:function  (invader) {
    
        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');
    
    },
    
    descend:function () {
    
        aliens.y += 10;
    
    },
    update: function() {
        if(flag_window==0){
            stateText3.visible=false;
            this.black0.y=0;
            this.enter.y=550;
            this.black.x=-10000;
            stateText.visible = false;
            if(this.startgame.isDown){
                flag_window=1;
                // this.player.x = 100;
                // this.player.y = 300;
                this.enter.y = -500;
                this.black0.y= -2000;
                this.restart();
            }
        }
        else if(flag_window==1){
                stateText3.visible=false;
            // if(pasue_flag==1){
                if(player.x<0) player.x=0;
                if(player.x>800) player.x=800;
                if(player.y<0) player.y=0;
                if(player.y>600) player.y=600;

                leveltime+=0.001;
                // console.log(leveltime);
                // if(this.pasue.isDown && nextpause<game.time.now && !this.game.pasued){
                //     this.game.paused = true;
                //     // pasue_flag=0;
                //     nextpause = game.time.now + 200;
                //     // game.input.Phaser.Keyboard.addOnce(()=>{this.game.paused=false},this);
                //     // aliens.setAll('body.velocity.x',0);
                //     // enemyBullets.setAll('body.velocity.x',0);
                //     // enemyBullets.setAll('body.velocity.y',0);
                // }
                if(this.voulumeup.isDown){
                    this.game.sound.volume += 0.02;
                }
                if(this.voulumedown.isDown){
                    this.game.sound.volume -= 0.02;
                }

                if(leveltime<0.1){
                    stateText2.text = " Level 1 ";
                    stateText2.visible = true;
                    aim.x=800;
                    aim.y=50;
                    aim.body.velocity.x=-100;
                }
                else if(leveltime>1 && leveltime<1.1){
                    stateText2.text = " Level 2 ";
                    stateText2.visible = true;
                    aim.x=800;
                    aim.y=300;
                    aim.body.velocity.x=-100;
                }
                else if(leveltime>2 && leveltime<2.1){
                    stateText2.text = " Level 3 ";
                    stateText2.visible = true;
                    aim.x=800;
                    aim.y=500;
                    aim.body.velocity.x=-100;
                }
                else if(leveltime>3 && leveltime<3.1){
                    stateText2.text = " Boss ";
                    stateText2.visible = true;
                    boss.x=700;
                    boss.y=300;
                }
                else if(leveltime>3.1 && leveltime<3.2){
                    stateText2.visible=false;
                    boss.body.velocity.y = 300;
                    if(boss.y<0) boss.y=0;
                    if(boss.y>600) boss.y=600;
                }
                else if(leveltime>3.2 && leveltime<3.3){
                    stateText2.visible=false;
                    boss.body.velocity.y = -300;
                    if(boss.y<0) boss.y=0;
                    if(boss.y>600) boss.y=600;
                }
                else if(leveltime>3.3 && leveltime<3.4){
                    stateText2.visible=false;
                    boss.body.velocity.y = 300;
                    if(boss.y<0) boss.y=0;
                    if(boss.y>600) boss.y=600;
                }
                else if(leveltime>3.4 && leveltime<3.5){
                    stateText2.visible=false;
                    boss.body.velocity.y = -300;
                    if(boss.y<0) boss.y=0;
                    if(boss.y>600) boss.y=600;
                }
                else if(leveltime>3.5 && leveltime<3.6){
                    stateText2.visible=false;
                    boss.body.velocity.y = 300;
                    if(boss.y<0) boss.y=0;
                    if(boss.y>600) boss.y=600;
                }
                else if(leveltime>3.6 && leveltime<3.7){
                    stateText2.visible=false;
                    boss.body.velocity.y = -300;
                    if(boss.y<0) boss.y=0;
                    if(boss.y>600) boss.y=600;
                }
                else{
                    stateText2.visible = false;
                }
                starfield.tilePosition.x -= 2;
                this.createAliens();
                if (player.alive)
                {
                    //  Reset the player, then check for movement keys
                    player.body.velocity.setTo(0, 0);

                    if (cursors.left.isDown)
                    {
                        player.body.velocity.x = -300;
                    }
                    else if (cursors.right.isDown)
                    {
                        player.body.velocity.x = 300;
                    }
                    else if (cursors.up.isDown)
                    {
                        player.body.velocity.y = -300;
                    }
                    else if (cursors.down.isDown)
                    {
                        player.body.velocity.y = 300;
                    }

                    if(flag_ultimate==1 && this.ultimate.isDown){
                        this.game.sound.play('boden2');
                        aliens.removeAll();
                        flag_ultimate=0;
                        this.ultimatelogo.y=-100;
                    }
                    //  Firing?
                    if (fireButton.isDown)
                    {
                        this.fireBullet();
                    }

                    if (game.time.now > firingTimer)
                    {
                        this.enemyFires();
                    }

                    //  Run collision
                    game.physics.arcade.overlap(bullets, aliens, this.collisionHandler, null, this);
                    game.physics.arcade.overlap(enemyBullets, player, this.enemyHitsPlayer, null, this);
                    game.physics.arcade.overlap(bullets, boss, this.collisionHandler2, null, this);
                    game.physics.arcade.overlap(aim,player, this.collisionHandler3, null, this);
                }
            // }
            // else if(pasue_flag==0){
            //     if(this.pasue.isDown && nextpause<game.time.now){
            //         pasue_flag=1;
            //         nextpause = game.time.now + 200;
            //         this.game.paused = false;
            //         // aliens.setAll('body.velocity.x',-100);
            //         // if(leveltime<=1){
            //         //     for(var i of enemyBullets){
            //         //         game.physics.arcade.moveToObject(i,player,200);
            //         //     }
            //         // }
            //         // else if(leveltime>1 && leveltime<=2){
            //         //     game.physics.arcade.moveToObject(enemyBullet,player,300);
            //         // }
            //         // else if(leveltime>2 && leveltime<=3){
            //         //     game.physics.arcade.moveToObject(enemyBullet,player,400);
            //         // }
            //     }
            // }
        }
        else if(flag_window==2){
            this.black.x=0;
            stateText2.visible=false;
            if(this.quit.isDown){
                flag_window=0;
            }
            if(this.continue.isDown){
                this.restart();
            }
        }
    },
    collisionHandler:function  (bullet, alien) {

        //  When a bullet hits an alien we kill them both
        bullet.kill();
        alien.kill();
        
        //  Increase the score
        score += 100;
        scoreText.text = scoreString + score + " vol Q:up W:down ";
    
        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(alien.body.x+50, alien.body.y+50);
        explosion.play('kaboom', 30, false, true);
    
        // if (aliens.countLiving() == 0)
        // {
        //     score += 1000;
        //     scoreText.text = scoreString + score;
    
        //     enemyBullets.callAll('kill',this);
        //     stateText.text = " You Won, \n Click to restart";
        //     stateText.visible = true;
    
        //     //the "click to restart" handler
        //     game.input.onTap.addOnce(this.restart,this);
        // }
    
    },
    collisionHandler3:function  (aim, player) {
        aim.body.x=-1000;
        aim.x=-1000;
        bullet = bullets.getFirstExists(false);
            
            if (bullet && livingEnemies.length > 0)
            {
                //  And fire it
                this.game.sound.play('boden');
                bullet.reset(player.x, player.y + 8);
                game.physics.arcade.moveToObject(bullet,livingEnemies[game.rnd.integerInRange(0,livingEnemies.length-1)],1000);
            }
    },
    collisionHandler2:function  (bullet, boss) {

        
    
        if (bosslives>0)
        {
            bosslives=bosslives-1;
        }
        
        boss.kill();

        if(bosslives<1){
            score += 3000;
            flag_window=2;
            stateText.text="A:quit S:restart     You Win!";
            stateText.visible = true;
            flag_window=2;
            winflag=1;
            enemyBullets.callAll('kill');
            bullets.callAll('kill');
            firebase.database().ref('game_score').push({
                username: localStorage.getItem("playerName") ,
                score: score
            }); 

            var postsRef = firebase.database().ref('game_score');
            var flagflag=0;
            
            postsRef.orderByChild("score").limitToLast(8).on("child_added", function(snapshot) {
                var obj=snapshot.val();
                stateText3.text += obj.username +' : '+ obj.score + '\n';
                stateText3.visible = true;
                if(flagflag==0){
                    stateText3.text = "";
                    flagflag=1;
                }
                console.log(obj);
              });
            
        }
        
        
        scoreText.text = scoreString + score + " vol Q:up W:down ";

        var explosion = explosions.getFirstExists(false);
        explosion.reset(boss.body.x+50, boss.body.y+50);
        explosion.play('kaboom', 30, false, true);
    
    },
    
    enemyHitsPlayer:function  (player,bullet) {
        
        bullet.kill();
    
        live = lives.getFirstAlive();
    
        if (live)
        {
            live.kill();
        }
    
        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x+50, player.body.y+50);
        explosion.play('kaboom', 30, false, true);
    
        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            enemyBullets.callAll('kill');
            bullets.callAll('kill');
            stateText.text="A:quit S:restart     You Lose!";
            stateText.visible = true;
            flag_window=2;
            
            firebase.database().ref('game_score').push({
                username: localStorage.getItem("playerName") ,
                score: score
            }); 
            winflag=0;
            // var temp1=0;
            // var tempname="";
            // var postsRef = firebase.database().ref('game_score');
            // postsRef.once('value')
            //     .then(function (snapshot) {
            //         var obj = snapshot.val();
            //         for(var key in obj){
            //             if(obj[key].score>temp1){
            //                 temp1=obj[key].score;
            //                 tempname=obj[key].username;
            //             }
            //         }
            //         stateText3.text = tempname +":"+ temp1+"points";
            //         stateText3.visible = true;
            //     })
            //     .catch(e => console.log(e.message));

            var postsRef = firebase.database().ref('game_score');
            var flagflag=0;
            
            postsRef.orderByChild("score").limitToLast(8).on("child_added", function(snapshot) {
                var obj=snapshot.val();
                stateText3.text += obj.username +' : '+ obj.score + '\n';
                stateText3.visible = true;
                if(flagflag==0){
                    stateText3.text = '';
                    flagflag=1;
                }
                console.log(obj);
              });
            
        }
    
    },
    
    enemyFires: function () {
    
        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);
    
        livingEnemies.length=0;
    
        aliens.forEachAlive(function(alien){
    
            // put every living enemy in an array
            livingEnemies.push(alien);
        });
    
    
        if (enemyBullet && livingEnemies.length > 0 && leveltime<=1)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);
            
            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(enemyBullet,player,200);
            firingTimer = game.time.now + 750;
        }

        if (enemyBullet && livingEnemies.length > 0 && leveltime>1&&leveltime<=2)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);
    
            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(enemyBullet,player,300);
            firingTimer = game.time.now + 750;
        }

        if(enemyBullet && livingEnemies.length > 0 && leveltime>2&&leveltime<=3){
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);
    
            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(enemyBullet,player,400);
            firingTimer = game.time.now + 750;
        }

        if(leveltime>3.1){
    
            // randomly select one of them
            var shooter=boss;
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(enemyBullet,player,400);
            firingTimer = game.time.now + 300;
        }
    
    },
    
    fireBullet:function  () {
    
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            bullet = bullets.getFirstExists(false);
            
            if (bullet)
            {
                //  And fire it
                this.game.sound.play('boden');
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.x = +400;
                bulletTime = game.time.now + 200;
            }
        }
    
    },
    
    resetBullet:function  (bullet) {
    
        //  Called if the bullet goes out of the screen
        bullet.kill();
    
    },
    
    restart:function () {
        //  A new level starts
        flag_window=1;
        this.black.x=-10000;
        score=0;
        scoreText.text = scoreString + score +" vol Q:up W:down ";
        leveltime=0.0;
        flag_ultimate=1;
        bosslives=10;
        this.ultimatelogo.y=30;
        //resets the life count
        lives.callAll('revive');
        //  And brings the aliens back from the dead :)
        aliens.removeAll();
    
        //revives the player
        player.revive();
        player.reset(0, 300);
        //hides the text
        stateText.visible = false;
    
    }
};
var game = new Phaser.Game(800, 600, Phaser.AUTO, "canvas")
game.state.add('main', mainState);
game.state.start('main');




