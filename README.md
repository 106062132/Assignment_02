# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : [空白鍵為發射子彈，總共有三關，每到下一關難度就會提高，提高的方法為增快敵人子彈的速度，（三關之後有一個boss，會在後面說明），只有一次使用大絕招的機會（畫面右上方的火箭圖示），按下Z之後可以將場上非boss的所有敵人消除（不會加分，因為最厲害的玩家不用用絕招）。]
2. Animations : [利用spritesheet做出，可以看到player的螺旋槳轉動。]
3. Particle Systems : [當物體被子彈打到時，會出現爆炸效果。]
4. Sound effects : [第一個是當player射出子彈時，發出音效，第二個是按下z時（大絕招），發出不同音效，可以在畫面上方看到調整聲音的方式（Q:提高音量，W:降低音量）]
5. Leaderboard : [剛進入網站時，會跳出對話視窗，可以輸入玩家名稱，在Win或Lose時（結束畫面），把分數前幾名的列出來（firebase）]
6. UI:[Gamepause要按下畫面上方的暫停圖示（再按一下解除暫停），其他都很明顯所以沒有多加說明。]

# Bonus Functions Description : 
1. [Bullet_automatic_aiming] : [在每關開始時，會有一個道具可以拿（黑色的子彈），拿到的瞬間，會將子彈自動射出到敵人身上。]
2. [Boss] : [最後一關是Boss關，Boss會快速上下移動，並且射出連發的子彈，這個Boss總共有10條命。]
